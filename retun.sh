#!/usr/bin/env bash


LOCAL_DIR=`dirname $0`;
source $LOCAL_DIR/vars.sh

function replace(){
    local OLD_VALUE=$1
    local NEW_VALUE=$2
    local FILE=$3

    sed -i "s/$OLD_VALUE/$NEW_VALUE/g" $FILE
}

replace $HOSTNAME HOSTNAME Vagrantfile
replace $CUSTOM_IP CUSTOM_IP Vagrantfile
replace $FULLHOSTNAME FULLHOSTNAME $LOCAL_DIR/puppet/environments/production/modules/baseconfig/files/puppet.conf
replace $FULLHOSTNAME FULLHOSTNAME $LOCAL_DIR/puppet/environments/production/modules/baseconfig/manifests/init.pp
replace $HOSTNAME HOSTNAME $LOCAL_DIR/puppet/environments/production/modules/baseconfig/manifests/init.pp
