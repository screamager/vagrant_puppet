#!/usr/bin/env bash


DOMAIN="example.local"
HOSTNAME="deb-puppet"
FULLHOSTNAME="${HOSTNAME}.$DOMAIN"
CUSTOM_IP="192.168.15.10"
