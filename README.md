This is vagrant use provision puppet for deploy puppetmaster with hiera and r10k.


It's need for mount environments (type filesystem "vboxsf" on guest os):
```
sudo apt install virtualbox-guest-additions-iso
```
It's add support "virtualbox-guest-additions-iso" in vagrant (conflicting with plugin libvirt):
```
sudo vagrant plugin install vagrant-vbguest
```

You need set your variable into file "vars.sh".

The script "build.sh" creates virtual machine and apply puppet tasks.

The script "return.sh" return the template values into files.

It's after done "build.sh" you able connect over ssh:
```
sudo vagrant ssh
```

The example task configure ntp may be apply command:
```
sudo puppet agent -t
```

