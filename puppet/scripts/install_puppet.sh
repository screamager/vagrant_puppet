#!/usr/bin/env bash

if [ ! -f "/etc/systemd/system/multi-user.target.wants/puppet.service" ]; then
    apt-get update
    apt-get install -y puppet
    systemctl start puppet.service
    systemctl enable puppet.service
fi
