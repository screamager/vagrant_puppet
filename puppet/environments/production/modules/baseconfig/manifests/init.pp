# == Class: baseconfig
#
# Performs initial configuration tasks for all vagrant boxes.
#

class baseconfig {

# Global path
  Exec { path => [ '/bin/', '/sbin/' , '/usr/bin/', '/usr/sbin/' ] }

  exec { 'apt update':
    command => 'apt update',
  }

# sh as bash
  exec { 'link sh to bash':
    command => 'ln -sf /bin/bash /bin/sh',
  }

  host { 'add the name to /etc/hosts':
    name    => 'HOSTNAME',
    host_aliases => 'FULLHOSTNAME',
    ip      => '127.0.0.1',
  }

# Install needed package (because puppet package with error)
 exec { 'apt install puppetmaster r10k':
    command => 'apt -q -y -o DPkg::Options::=--force-confold install  puppetmaster r10k',
  }

# Configure pappet
  file { '/etc/puppet/puppet.conf':
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/baseconfig/puppet.conf',
  }

  file { '/etc/puppet/code/environments':
    ensure => 'directory',
    owner  => 'root',
    group  => 'puppet',
    mode   => '2775',
  }

# Config hiera
  file { '/etc/puppet/hiera.yaml':
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/baseconfig/hiera.yaml',
  }

  exec { 'link hiera.yaml':
    command => 'ln -sf /etc/puppet/hiera.yaml /etc/hiera.yaml',
  }

# config r10k
  exec { 'link for pupperlabs':
    command => 'ln -sf /etc/puppet /etc/puppetlabs',
  }

  file { '/etc/puppetlabs/r10k':
    ensure => 'directory',
    owner  => 'root',
    group  => 'puppet',
    mode   => '0775',
  }

  file { '/var/cache/r10k':
    ensure => 'directory',
    owner  => 'root',
    group  => 'puppet',
    mode   => '2775',
  }

  file { '/etc/puppetlabs/r10k/r10k.yaml':
    owner  => 'root',
    group  => 'puppet',
    mode   => '0644',
    source => 'puppet:///modules/baseconfig/r10k.yaml',
  }

# Restart puppet server
  exec { 'restart puppetmaster':
    command => 'systemctl restart puppetmaster.service',
  }

# Create git repo
  exec { 'git init puppet.git':
    command => 'git init --bare --shared=group /srv/puppet.git',
  }
  exec { 'chgrp puppet.git':
    command => 'chgrp -R puppet /srv/puppet.git',
  }
  exec { 'git symbolic-ref':
    cwd     => '/srv/puppet.git',
    command => 'git symbolic-ref HEAD refs/heads/production',
  }

  file { '/srv/puppet.git/hooks/post-receive':
    owner  => 'root',
    group  => 'puppet',
    mode   => '0755',
    source => 'puppet:///modules/baseconfig/post-receive',
  }

# Firsts task for puppet from the user vagtant

  exec { 'add user vagrant to group puppet':
    command => '/usr/sbin/adduser vagrant puppet',
  }
  exec { 'git clone':
    user    => 'vagrant',
    cwd     => '/home/vagrant',
    command => 'git clone /srv/puppet.git',
  }
  exec { 'mkdir hieradate/nodes manifests site':
    user    => 'vagrant',
    cwd     => '/home/vagrant/puppet',
    command => 'mkdir -p hieradata/nodes manifests site; touch site/.keep hieradata/nodes/.keep',
  }
  exec { 'echo hiera_include(..) > site.pp':
    user    => 'vagrant',
    cwd     => '/home/vagrant/puppet',
    command => 'echo hiera_include\(\'classes\'\) > manifests/site.pp',
  }

  exec { 'Pappetfile ntp':
    user    => 'vagrant',
    cwd     => '/home/vagrant/puppet',
    command => 'echo -e "mod \'puppetlabs/ntp\', \'6.1.0\'\nmod \'puppetlabs/stdlib\', :latest\n" > Puppetfile',
  }
  exec { 'hieradata/common.yaml':
    user    => 'vagrant',
    cwd     => '/home/vagrant/puppet',
    command => 'echo -e "---\nclasses:\n  - ntp\n\nntp::servers:\n  - 0.pool.ntp.org\n" > hieradata/common.yaml',
  }

  exec { 'git commit':
    user    => 'vagrant',
    cwd     => '/home/vagrant/puppet',
    command => 'git checkout -b production; git add *; git commit -a -m "Initial commit."; git push -u origin production',
  }

}
